fn main() {
    let mut best_len = 0;
    let mut best_number = 0;
    let mut number = 0;

    while best_len < 10 {
        number+=1;
        
        let curr_len = pers_mult(number_to_digits(number), 0);
        if curr_len > best_len {
            best_len = curr_len;
            best_number = number;
            println!("New best {} with {} steps", best_number, best_len)
        }
    }
    println!("Best was {} with {} steps", best_number, best_len)
}

fn pers_mult(digits: Vec<i32>, stack_index: i32) -> i32 {
    let mut num = 1;
    for digit in digits {
        num *= digit;
    }
    let digits_new = number_to_digits(num);
    if digits_new.len() == 1 {
        return stack_index
    } else {
        return pers_mult(number_to_digits(num), stack_index+1)
    }
}

fn number_to_digits(number: i32) -> Vec<i32> {
    let mut digits = Vec::new();
    let mut temp_num = number;
    let mut power_num: u32 = 0;
    while temp_num > 0 {
        let digit = (temp_num/(10_i32.pow(power_num)))%10;
        temp_num -= digit*(10_i32.pow(power_num));
        digits.push(digit);
        power_num += 1;
    }

    return digits;
}